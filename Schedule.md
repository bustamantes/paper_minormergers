* Go to z=0 and get the IDs of the stars in the main galaxy. I think something like the following will do the job:

```
Attributes = T.GetSubhaloParticles(127,0,Attrs=['Coordinates', 'GFM_InitialMass', 'GFM_StellarFormationTime','ParticleIDs'],Type=4)
IDs = Attributes['ParticleIDs']
```


* Go to z=1 and pick out the stars that end up in the main galaxy at z=0. Match the IDs from step #1 with the IDs from all the stars at z=1. When reading in the z=1 stars, you want to first read in all stars from the snapshot. This can be done with this function:

```
Attributes = T.GetParticles(40,Attrs=['Coordinates','ParticleIDs'],Type=4)#replace 40 with the number of the snapshot at z=1    
IDs = Attributes['ParticleIDs']
```

  You will have to use the numpy.in1d function to match z=0 and z=1 IDs of the stars.

  After that you can try to make a x-y plot of the stars at z=1 that ends up at z=0.

  Does this step make sense?

  (My repository contains a parallel version of this script (In Misc/in1d_parallal.py). Maybe you will need this, but start with the normal in1d.)


* At z=1 determine the mass of the galaxies that later end up in the main galaxy at z=0. When you're finished with #1 and #2 we can talk about this. While you do #1 and #2 I will try to code something up for this.

